# some silly block functions

#reverser
def reverser(&proc1)
  proc1.call.split(" ").map {|word| word.reverse}.join(" ")
end

def adder(n = 1, &proc1)
  proc1.call + n
end

def repeater(n = 1, &proc1)
  n.times.each do |i|
    proc1.call
  end
end
