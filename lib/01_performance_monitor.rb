def measure(n = 1, &proc1)
  start = Time.now
  n.times.each do |i|
    proc1.call
  end
  endtime = Time.now
  (endtime - start) / n
end
